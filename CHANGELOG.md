
## 0.0.19 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/servicenow-change-request-creation!4

---

## 0.0.18 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/servicenow-change-request-creation!1

---

## 0.0.17 [05-05-2021]

* Update README.md, images/servicenow_change_request_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-change-request-creation!12

---

## 0.0.16 [03-23-2021]

* Update README.md, images/servicenow_change_request_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-change-request-creation!12

---

## 0.0.15 [03-23-2021]

* Update README.md, images/servicenow_change_request_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-change-request-creation!12

---

## 0.0.14 [03-01-2021]

* Update README.md, images/servicenow_change_request_creation_canvas.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-change-request-creation!12

---

## 0.0.13 [03-01-2021]

* patch/2021-03-01T15-42-08

See merge request itential/sales-engineer/selabprebuilts/servicenow-change-request-creation!11

---

## 0.0.12 [02-26-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/create-requestsnow!7

---

## 0.0.11 [02-26-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/create-requestsnow!5

---

## 0.0.10 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-requestsnow!4

---

## 0.0.9 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-requestsnow!4

---

## 0.0.8 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-requestsnow!4

---

## 0.0.7 [02-24-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-requestsnow!1

---

## 0.0.6 [02-22-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-requestsnow!1

---

## 0.0.5 [02-19-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-requestsnow!1

---

## 0.0.4 [02-19-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-requestsnow!1

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-requestsnow!1

---

## 0.0.2 [02-17-2021]

* Bug fixes and performance improvements

See commit 6cca533

---
