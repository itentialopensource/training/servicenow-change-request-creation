<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# ServiceNow Change Request Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Automation Catalog and JSON-Form](#automation-catalog-and-json-form)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview
This Pre-Built integrates with the [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow) to create a Change Request in the `change_request` table in ServiceNow.

## Automation Catalog and JSON-Form
This workflow has an [Automation Catalog (AC) Item](./bundles/ac_agenda_jobs/ServiceNow%20Change%20Request%20Creation.json) that calls a workflow. The AC Item uses a JSON-Form to specify common fields populated when an Change Request is created. The workflow the AC Item calls queries data from the formData job variable.

<table>
  <tr>
    <td>
      <img src="./images/servicenow_change_request_creation_ac_form.png" alt="form" width="800px">
    </td>
  </tr>
  <tr>
    <td>
      <img src="./images/servicenow_change_request_creation_canvas.png" alt="form" width="800px">
    </td>
  </tr>
</table>

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2021.1`
- [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow)
  - `^2.2.0`

## Requirements

This Pre-Built requires the following:

- A [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow).
- A ServiceNow account that has write access to the `change_request` table.

## Features

The main benefits and features of the Pre-Built are outlined below.

- Provide the fields required to create a ServiceNow Change Request.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Automation Catalog Item `ServiceNow Change Request Creation` or call [ServiceNow Change Request Creation](./bundles/workflows/ServiceNow%20Change%20Request%20Creation.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "notificationTitle": "The Change Request notification title",
  "summary": "The Change Request summary",
  "waitTaskMessage": "The Change Request wait task message",
  "backoutPlan": "The Change Request backout plan",
  "testPlan": "The Change Request test plan",
  "implementationPlan": "The Change Request implementation plan"
}
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
